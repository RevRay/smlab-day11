package ru.smlab.school.day11.classwork;

public class Card {
    String bankName;
    String cardNumber;
    int cvvCode;

    public Card(String bankName, String cardNumber, int cvvCode) {
        this.bankName = bankName;
        this.cardNumber = cardNumber;
        this.cvvCode = cvvCode;
    }

    @Override
    public String toString() {
        return "Card{" +
                "bankName='" + bankName + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", cvvCode=" + cvvCode +
                '}';
    }

    public boolean pay(int sum){
        return false;
    }
}
