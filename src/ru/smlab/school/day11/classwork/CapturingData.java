package ru.smlab.school.day11.classwork;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CapturingData {
    public static void main(String[] args) {
        String cardsData =
                "Оплата произведена дебетовой картой банка \"УралсибБанк\", номер 9000 4343 5432 1090 с использованием кода CVV/CVC 491\n" +
                "Оплата произведена кредитной картой банка \"ПАО Росбанк\", номер 9080 2116 5474 2372 с использованием кода CVV/CVC 065\n" +
                "Оплата произведена кредитной картой банка \"ПАО ВТБ\", номер 4532 2880 6001 7720 с использованием кода CVV/CVC 044\n" +
                "Оплата произведена кредитной картой банка \"ПАО ВТБ\", номер 4563 2930 0046 1117 с использованием кода CVV/CVC 155";

        //вытащить из строк всю информацию о каждой из карт. превратить в объекты Card
        String cardRegexp = ".*?\"([а-яА-Я\\s]+)\", номер ((\\d{4}\\s){4}).*?CVC\\s(\\d{3})";

        Pattern p = Pattern.compile(cardRegexp);
        Matcher m = p.matcher(cardsData);

        Card[] foundCards = new Card[4];



//        for(int i = 0; m.find(); i++) {
//            //System.out.println("Первая подстрока, соответствующая регулярке: " + m.group(0));
//            foundCards[i] = new Card(m.group(1), m.group(2), Integer.parseInt(m.group(4)));
//        }
//
//        System.out.println(Arrays.toString(foundCards));

        while(m.find()){
            for(int i=0; i < m.groupCount(); i++) {
                System.out.println(m.group(i));
            }
        }
    }
}
