package ru.smlab.school.day11.classwork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindingData {
    public static void main(String[] args) {
        String unprocessedData =
                "Первое место по продажам в 2020 году заняла компания 'Toyota'." +
                "Вторую строчку в рейтинге удерживает концерн 'Volkswagen AG'." +
                "За ним с некоторым отставанием следует 'Ford'.";

        //найти в строке и возвратить все фигурирующие названия
        //автопроизводителей-лидеров рынка

        String[] results = new String[3];

        // * vs *?  + vs +?   greedy   lazy
        String regexpAuto = ".*?'([\\w\\s]+)(')\\.";

        Pattern p = Pattern.compile(regexpAuto);
        Matcher m = p.matcher(unprocessedData);

        while (m.find()) {
            System.out.println(m.group(0));
            System.out.println(m.group(1));
            System.out.println(m.group(2));
            m.group(1);
        }
    }
}
