package ru.smlab.school.day11.classwork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpMatchesClasswork {

    public static void main(String[] args) {

        //отобрать только трехбуквенные коды валют (латиница)
        String[] stateNames = new String[]{
                "RUB",
                "CAD",
                "USD",
                "Dollar",
                "KRW",
                "EUR",
                "124",
                "M L",
                "рубль",
                ""
        };

        String regexp = "[a-zA-Z]{3}";
        for (String s : stateNames) {
            if (s.matches(regexp)) {
                System.out.println(s);
            }
        }



        //отобрать только ФИО или неполное ФИО
        String[] names = new String[]{
                "Карпов Петр", //да
                "78 90 56",
                "что-то не совсем похожее на имя",
                "Петров Иван Евгеньевич", //да
                "это не имя",
                "Астахов Николай (28лет)", //нет
                ""
        };

        String nameRegex = "([А-Я][а-я]*\\s?){2,3}"; //
        for (String s : names) {
            if (s.matches(nameRegex)) {
                System.out.println(s);
            }
        }

        //отобрать только сообщения на русском языке, без цифр
        String[] messages = new String[]{
                "Не ругайтесь на ленивых людей, они ничего не сделали.",
                "Wake up, Neo. The matrix has you",
                "12 + 4 = 21",
                "",
                "Лучше сделать идеально и никогда, чем сегодня, но кое-как."
        };

        String reg = "([а-яА-Я-]*,?\\s?)*\\.";
        for (String s : messages) {
            if (s.matches(reg)) {
                System.out.println(s);
            }
        }

        String[] phoneNumbers = new String[]{
                "+79032483700",
                "89654341291",
                "not a number",
                "3.14159",
                "1024",
                "79654341291",
                "8(996)0674181",
                "не телефонный номер",
                "89014540156",
                "+89014540156",
                "89268009090 (доступен по вторникам)",
                "",
                "*101#",
                "+7(905)229-08-07",
        };

        String phoneRegexp = "((\\+?7)|(8))\\(?\\d{3}\\)?\\d{3}-?\\d{2}-?\\d{2}";
        for (String s : phoneNumbers) {
            if (s.matches(phoneRegexp)) {
                System.out.println(s);
            }
        }


        String[] emails = new String[]{
            "d6f@ya.ru",
            "support-inbox@deliveryworld.com",
            "@vasya.net",
            "orders@@aliexpress.com",
            "vasilev87@gmail.com",
            "ivanovpetr@rambler.ru",
            "tickets@sheremetyevo.aero",
            "ab@.ru",
            ""
        };

        String emailRegexp = "[\\w-]+@[a-zA-Z-\\.0-9]+\\.[a-zA-Z]{2,10}"; // \\w = [a-zA-Z0-9_]
        for (String s : emails) {
            if (s.matches(emailRegexp)) {
                System.out.println(s);
            }
        }

        String[] ipAddresses = new String[]{
                "10.0.11.41",
                "28.126.11", //
                "243.256.112.0", //
                "11.12.109.xxx", //
                "203.26..", //
                "2OO.OO.lO.2l", //
                "127.0.0.1",
                "8.8.8.8",
        };

//        String ipRegex = "(\\d{1,3}\\.){3}\\d{1,3}";
        String ipRegex = "(([2][0-4][0-9])|([2][5][0-5]|([0-1]?[0-9]?[0-9]))\\.){3}(([2][0-4][0-9])|([2][5][0-5]|([0-1]?[0-9]?[0-9])))";
        for (String s : ipAddresses) {
            if (s.matches(ipRegex)) {
                System.out.println(s);
            }
        }

        String str = "abc";
        String regexp1 = "\\w{3}";

        Pattern p = Pattern.compile(regexp1); //объект, хранящий регулярку
        Matcher m = p.matcher(str);

        System.out.println(m.matches());

    }
}
